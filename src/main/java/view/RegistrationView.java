package view;

import entity.AccountInfo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.web.context.annotation.RequestScope;
import services.RegistrationServices;

import javax.annotation.PostConstruct;
import java.util.List;

/**
 * Created by ADI on 6/26/2017.
 */

@RequestScope
@Component
public class RegistrationView {
    private AccountInfo accountInfo;

    @Autowired
    private RegistrationServices registrationServices;

    private List<AccountInfo> registeredUsers;

    @PostConstruct
    public void init() {reset();}

    public String save() {
        registrationServices.register(accountInfo);

        return "thankyou.xhtml";
    }

    public List<AccountInfo> getRegisteredUsers() {
        return registrationServices.findAll();
    }

    private void reset() {
        accountInfo = new AccountInfo();
    }

    public AccountInfo getAccountInfo() {
        return accountInfo;
    }

    public void setAccountInfo(AccountInfo accountInfo) {
        this.accountInfo = accountInfo;
    }
}
