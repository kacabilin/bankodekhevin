package controller;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;


/**
 * Created by ADI on 6/25/2017.
 */

@Controller
public class WelcomePageController {
    @GetMapping("/")
    public String welcomePage(){
        return "index.xhtml";
    }
}
