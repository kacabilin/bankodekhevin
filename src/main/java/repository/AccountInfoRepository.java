package repository;

import entity.AccountInfo;
import org.springframework.data.repository.CrudRepository;

import java.util.List;

/**
 * Created by ADI on 6/25/2017.
 */
public interface AccountInfoRepository extends CrudRepository<AccountInfo, Long> {
    List<AccountInfo> findAll();
}
