package services;

import entity.AccountInfo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import repository.AccountInfoRepository;

import java.util.List;

/**
 * Created by ADI on 6/26/2017.
 */
@Service
public class RegistrationServices {
    @Autowired
    private AccountInfoRepository accountInfoRepository;

    public void register(AccountInfo accountInfo) {
        accountInfoRepository.save(accountInfo);
    }

    public List<AccountInfo> findAll() {
        return accountInfoRepository.findAll();
    }
}
