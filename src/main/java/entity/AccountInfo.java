package entity;

import org.hibernate.validator.constraints.Length;
import org.hibernate.validator.constraints.NotEmpty;

import javax.persistence.*;
import java.io.Serializable;

/**
 * Created by ADI on 6/25/2017.
 */

@Entity
@Table(name = "bank")
public class AccountInfo implements Serializable{


    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id")
    private long id;

    @Column(name = "accountNo")
    @NotEmpty(message = "Please provide Account Number")
    private long accNo;

    @Column(name = "firstName")
    @NotEmpty(message = "Please provide your first name")
    private String fname;

    @Column(name = "lastName")
    @NotEmpty(message = "Please provide your last name")
    private String lname;

    @Column(name = "pin")
    @Length(min = 4,max = 4,message = "Please provide a 4 digit pin")
    @NotEmpty(message = "Please provide your 4 digit Pin")
    @Transient
    private int pin;

    @Column(name = "accountBalance")
    private int accbalance;

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public long getAccNo() {
        return accNo;
    }

    public void setAccNo(long accNo) {
        this.accNo = accNo;
    }

    public String getFname() {
        return fname;
    }

    public void setFname(String fname) {
        this.fname = fname;
    }

    public String getLname() {
        return lname;
    }

    public void setLname(String lname) {
        this.lname = lname;
    }

    public int getPin() {
        return pin;
    }

    public void setPin(int pin) {
        this.pin = pin;
    }

    public int getAccbalance() {
        return accbalance;
    }

    public void setAccbalance(int accbalance) {
        this.accbalance = accbalance;
    }
}
